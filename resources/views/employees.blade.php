@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Employees') }}</div>

                <div class="card-body">

                    @if(isset($newEmployee['name']))
                        <div class="alert alert-success">
                            <strong>Congratulation! </strong> <br>
                            You have new Employee {{$newEmployee['name']}}; <br>
                            You can Call at <a href="tel:{{$newEmployee['phone']}}">{{$newEmployee['phone']}}</a> or send Email at <a href="mailto:{{$newEmployee['email']}}">{{$newEmployee['email']}}</a>
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Phone')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($employees as $key=>$val)

                            <tr>
                                <td>{{$val['name']}}</td>
                                <td>{{$val['email']}}</td>
                                <td>{{$val['phone']}}</td>
                            </tr>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
