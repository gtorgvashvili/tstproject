<?php

namespace App\Http\Controllers;

use App\Candidates;
use App\Employees;
use App\Property;
use App\User;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    /**
     * Show candidates page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function candidates()
    {



        $candidates = Candidates::all()->map->toArray();

        return view('candidates', ['candidates'=>$candidates]);
    }


    /**
     * Show employees page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function employees($newemployee = 0)
    {

        $newEmployee = $this->qualify($newemployee);

        $employees = Employees::all()->map->toArray();

        return view('employees', ['employees'=>$employees, 'newEmployee'=>$newEmployee]);
    }


    /**
     * Show employees page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function qualify($newEmployeeId = 0)
    {
        if(!$newEmployeeId || !is_numeric($newEmployeeId))return false;

        $candidate = Candidates::find($newEmployeeId);

        if(!$candidate)return false;

        $newEmployee = new Employees();
            $newEmployee->name = $candidate->name;
            $newEmployee->email = $candidate->email;
            $newEmployee->phone = $candidate->phone;
        $newEmployee->save();

        Candidates::destroy($newEmployeeId);


        return $candidate->toArray();
    }

    /**
     * Show filtered users
     *
     * @return all properties related to agents view
     */
    public function agents($status = 0){

        $property = new Property;
        $agents = $property->agents();

        return view('agents', ['agents'=>$agents]);
    }


}
