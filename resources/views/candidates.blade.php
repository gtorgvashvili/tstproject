@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Candidates') }}</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Phone')}}</th>
                            <th>{{__('Action')}}</th>
                        </thead>
                        <tbody>
                        @foreach ($candidates as $key=>$val)

                            <tr>
                                <td>{{$val['name']}}</td>
                                <td>{{$val['email']}}</td>
                                <td>{{$val['phone']}}</td>
                                <td><a href="{{ route('employees', ['id' => $val['id']]) }}" class="btn btn-success btn-sm">{{__('Qualify')}}</a></td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
