@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Candidates') }}</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Email')}}</th>
                            <th>{{__('Role')}}</th>
                            <th>{{__('Property type')}}</th>
                            <th>{{__('Points')}}</th>
                            <th>{{__('Status')}}</th>
                        </thead>
                        <tbody>
                        @foreach ($agents as $key=>$val)

                            <tr>
                                <td>{{$val->name}}</td>
                                <td>{{$val->email}}</td>
                                <td>{{$val->role_name}}</td>
                                <td>{{$val->property_name}}</td>
                                <td>{{$val->points}}</td>
                                <td>{{$val->status}}</td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
