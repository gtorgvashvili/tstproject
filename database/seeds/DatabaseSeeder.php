<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('candidates')->delete();
        DB::table('employees')->delete();
        DB::table('role')->delete();
        DB::table('user_role')->delete();
        DB::table('property')->delete();
        DB::table('user_property')->delete();

        /// seed admin user
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'status' => 'active',
            'points' => 90,
        ]);

        /// seed roles
        DB::table('role')->insert([['name' => 'agent'], ['name' => 'owner']]);

        /// seed properties
        DB::table('property')->insert([['name' => 'land'], ['name' => 'penthouse'], ['name' => 'home']]);

        /// user statuses
        $statuses = ['active','inactive','deleted'];

        /// myltiple seeding
        for($i =0; $i<=50; $i++){
            /// seed users / agents
            $name = strtolower( Str::random(5) );
            DB::table('users')->insert([
                'name' => $name,
                'email' => $name.'@gmail.com',
                'password' => Hash::make('password'),
                'status' => $statuses[array_rand($statuses)],
                'points' => rand(1,255),
            ]);

            /// seed user role relationships
            DB::table('user_role')->insert([
                'user_id' => rand(1,50),
                'role_id' => rand(1,2),
            ]);

            /// seed user property relationship
            DB::table('user_property')->insert([
                'user_id' => rand(1,50),
                'property_id' => rand(1,3),
            ]);


            /// seed candidates
            $name = strtolower( Str::random(3) );
            DB::table('candidates')->insert([
                'name' => $name,
                'email' => $name.'@gmail.com',
                'phone' => rand(1000000000, 9999999999),
            ]);


            /// seed employees
            $name = strtolower( Str::random(4) );
            DB::table('employees')->insert([
                'name' => $name,
                'email' => $name.'@gmail.com',
                'phone' => rand(1000000000, 9999999999),
            ]);

        }


    }
}
