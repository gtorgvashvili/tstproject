<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Property extends Model
{
    //
    protected $table = 'property';
    protected $primaryKey = 'id';

    public function user(){
        return $this->belongsToMany('App\User', 'user');
    }

/**
* select all properties related to agents
*/
    public function agents(){

        $results = DB::select( DB::raw("select `users`.*, role.name as role_name, property.name as property_name from `property`
inner join `user_property` on `user_property`.`property_id` = `property`.`id`
inner join `users` on `users`.`id` = `user_property`.`user_id`
inner join `user_role` on `user_role`.`user_id` = `users`.`id`
inner join `role` on `role`.`id` = `user_role`.`role_id`
where `role`.`name` = 'agent' and `users`.`status` = 'active' and `users`.`points` >= 85"));

        /// group by users.id /// if need select just users with status agent


        return $results;

    }

}
